var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');

const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectID;
// liga del cluster Mongo Atlas
const CONNECTION_URL = "mongodb+srv://m001-student:pollito4@carlos-m001-z2fzn.mongodb.net/test?retryWrites=true&w=majority"
const DATABASE_NAME = "Practitioner";

var database, collection;

var connection = MongoClient.connect(CONNECTION_URL, { useNewUrlParser: true }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(DATABASE_NAME);
        collection = database.collection("Usuarios");
        console.log("Connected to `" + DATABASE_NAME + "`!");
    });

var app = express();
app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());

app.get('/', function(request, response) {
	//response.sendFile(path.join('public/login.html'));
  //response.sendfile('http://127.0.0.1:8081/components/demopolymer/');
  response.sendfile('./login.html');
});

app.post('/auth', function(req, res) {
	console.log("entro al post");
	var name = req.body.username;
	var password = req.body.password;
  console.log(name + '-'+ password)
  var query={'username': name};
	if (name && password) {
        collection.findOne({ username: name}, function(err, user) {
             if(user ===null){
               res.end("Login invalid");
               console.log("Login invalid");
             }else if (user.username == name && user.password == password){

               console.log(user.username+'-'+user.password)
               res.writeHead(301,{Location: 'http://127.0.0.1:8081/components/demopolymer/'});
               res.end();
               console.log('completeprofile',{profileData:user});
             } else {
               console.log(user.username+'-'+user.password)
               console.log("Credentials wrong");
               res.end("Login invalid");
          }
        });
	} else {
		res.send('Please enter Username and Password!');
		res.end();
	}
});

app.get('/home', function(request, response) {
	if (request.session.loggedin) {
		response.send('Welcome back, ' + request.session.username + '!');
	} else {
		response.send('Please login to view this page!');
	}
	response.end();
});

app.get('/registro.html', function(request, response) {
	response.sendfile('./registro.html');
});

app.post("/Add",function (req,resp) {
  console.log(req.body);
  collection.insertOne(req.body, function(err, res) {
    if (err){
      console.log("1 document inserted");
      resp.status(200).send();
    }else{
      console.log("1 document inserted");
      resp.status(200).send();
    }
  });
});

app.get('/map.html', function(request, response) {
	response.sendfile('./map.html');
});

app.listen(3000);
